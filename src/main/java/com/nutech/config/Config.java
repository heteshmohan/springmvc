package com.nutech.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.nutech.dao.UserDaoImp;

@Configuration
@PropertySource("classpath:Application.properties")
public class Config 
{
	@Value("${db.userName}")
	private String userName;
	@Value("${db.Password}")
	private String password;
	@Value("${db.driver}")
	private String dbDriver;
	@Value("${db.url}")
	private String url;
	
	@Bean
	  public DriverManagerDataSource ds() {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName(dbDriver);
		ds.setUrl(url);
		ds.setUsername(userName);
		ds.setPassword(password);
		return ds;
	  }
	@Bean 
	public JdbcTemplate jt()
	{
		JdbcTemplate jt = new JdbcTemplate();
		jt.setDataSource(ds());
		return jt;
	}
	
	@Bean
	public UserDaoImp dao() 
	{
		UserDaoImp dao = new UserDaoImp();
		dao.setTemplate(jt());
		return dao;
	} 
	
}
