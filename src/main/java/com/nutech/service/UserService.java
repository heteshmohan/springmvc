package com.nutech.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nutech.dao.UserDao;
import com.nutech.model.UserProfile;

@Service
public class UserService {
	
@Autowired	
UserDao dao;	
	
public void save(UserProfile profile) {	
	
	dao.save(profile);
}
public void delete(String id) {
	dao.delete(id);
}
public List<UserProfile> viewAll() {
	return dao.viewAll();
	
}
public UserProfile getUserById(int id) {
	return dao.getUserById(id);
}
public void update(UserProfile profile) {
	dao.update(profile);
	
}
}
