package com.nutech.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.nutech.model.UserProfile;
import com.nutech.service.UserService;

@Controller
public class UserController  {
	
	@Autowired
	private UserService service;
	
	@RequestMapping(value="/")
	public String home(ModelMap model)
	{
		model.addAttribute("message", "Hello Spring MVC Framework!");
		return "home";
	}
	
	@RequestMapping(value="/User" ,method=RequestMethod.GET)
	public String User(ModelMap model)
	{
		model.addAttribute("message", "Test jhfkuj");
		return "Test";
	}
	
	@RequestMapping("/empform")    
    public String showform(Model m){    
        m.addAttribute("command", new UserProfile());
        return "empform";   
    } 
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	 public String save(@ModelAttribute("profile") UserProfile Profile){  
	        service.save(Profile);    
	        return "redirect:empform";    
	    }
	
	@RequestMapping(value="/view",method=RequestMethod.GET)
	public ModelAndView  ViewAll()
	{
		ModelAndView model = new ModelAndView("userView");
		List <UserProfile>allUser = service.viewAll();
		model.addObject("USER", allUser);
		return model;
	}
	@RequestMapping(value="/Delete/{id}")
	public String delete(@PathVariable String id)
	{
		service.delete(id);
		return "redirect:./";
	}
	@RequestMapping(value="/Edit/{id}")
	public String edit(@PathVariable int id, Model m){ 
		UserProfile  user = service.getUserById(id);
		m.addAttribute("command",user); 
		return "userEditForm";
	}
	@RequestMapping(value="/SaveEdit" ,method=RequestMethod.POST)
	public String saveEdit(@ModelAttribute("profile") UserProfile profile)
	{
		service.update(profile);
		return "redirect:./"; 
	}
	
	
}


