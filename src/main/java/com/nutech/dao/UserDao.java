package com.nutech.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.nutech.model.UserProfile;

@Repository
public interface UserDao{
	public int save(UserProfile profile);
	public List <UserProfile> viewAll();
	public void delete(String id);
	public UserProfile getUserById(int id);
	public void update(UserProfile user);
}
