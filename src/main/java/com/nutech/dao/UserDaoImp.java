package com.nutech.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.nutech.model.UserProfile;

public class UserDaoImp implements UserDao {
	
	@Autowired
	JdbcTemplate template; 
	
	@Override
	public int save(UserProfile profile) {
		 String sql="INSERT INTO USERPROFILE values (seq_User.nextval,'"+profile.getName()+"','"+profile.getEmail()+"','"+profile.getPlace()+"')";
		 	System.out.println("done");
		    return template.update(sql); 
		    
	}

	@Override
	public List<UserProfile> viewAll() {
		return template.query("select * from USERPROFILE",(resultSet, i) -> {
            return toUser(resultSet);
	});
	}
	private UserProfile toUser(ResultSet resultSet) throws SQLException {
		UserProfile user = new UserProfile();
		user.setUserid(resultSet.getString("USER_ID"));
		user.setName(resultSet.getString("USER_NAME"));
		user.setEmail(resultSet.getString("USER_EMAIL"));
		user.setPlace(resultSet.getString("USER_PLACE"));
		return user;
		
	}

	@Override
	public void delete(String id) {
		String sql="delete from USERPROFILE where USER_ID ="+id;
		template.update(sql);
	}

	@Override
	public UserProfile getUserById(int id) {
		String sql = "select * from USERPROFILE where USER_ID ="+id;
		UserProfile profile;
		profile = (UserProfile) template.queryForObject(sql, new UserMapper());
		return profile;
	}

	@Override
	public void update(UserProfile user) {
		String sql = "update USERPROFILE set USER_ID='"+user.getUserid()+"',USER_NAME='"+user.getName()+"',USER_EMAIL='"+user.getEmail()+"',USER_PLACE='"+user.getPlace()+"' WHERE USER_ID= "+user.getUserid();
		template.update(sql);  
	}

	public JdbcTemplate getTemplate() {
		return template;
	}

	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}    
	
	
}
	
