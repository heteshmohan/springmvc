package com.nutech.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.nutech.model.UserProfile;

public class UserMapper implements RowMapper<UserProfile> {

	@Override
	public UserProfile mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserProfile user = new UserProfile();
		user.setUserid(rs.getString("USER_ID"));
		user.setName(rs.getString("USER_NAME"));
		user.setEmail(rs.getString("USER_EMAIL"));
		user.setPlace(rs.getString("USER_PLACE"));
		return user;
	}

}
